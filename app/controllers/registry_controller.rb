class RegistryController < ApplicationController

  def one
    @registry = Registry.find(params[:id])
    @data = {"data" => @registry}
    render json: @data
  end

  def list
    @registries = Registry.all
    @registry = {"registries" => @registries}
    render json: @registry
  end

  def create
    @registry = Registry.new(registry_params)
    if @registry.save
      render json: {status: "created :)"}
    else
      render json: {status: "bad :("}
    end
  end

  def update
    @name = params[:name]
    @registry = Registry.find(params[:id])
    @registry.name = @name
    if @registry.save
      render json: {status: "updated"}
    else
      render json: {status: "bad :("}
    end
  end

  def destroy
    @registry = Registry.find(params[:id])
    @registry.destroy
    render json: {status: "deleted"}
  end

  private

  def registry_params
    params.require(:registry).permit(:name, :email, :guest, :song)
  end
end
