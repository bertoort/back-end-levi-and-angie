class AddColumnsToRegistry < ActiveRecord::Migration
  def change
    add_column :registries, :email, :string, null: false
    add_column :registries, :guest, :boolean, default: false
    add_column :registries, :song, :string
  end
end
